import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./screens/home/Home";
import Blog from "./screens/blog/Blog";
import About from "./screens/about/About";
import Contact from "./screens/contact/Contact";
import EditBlog from "./screens/blog/EditBlog";
import Wrapper from "./components/Wrapper";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<Wrapper />}>
          <Route path="/" element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/blog/:id" element={<Blog />} />
          <Route path="/blog/edit/:id" element={<EditBlog />} />
          <Route path="/blog/new" element={<EditBlog />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
