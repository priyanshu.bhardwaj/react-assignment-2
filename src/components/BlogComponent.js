import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { TbEdit, TbArrowUpRight } from "react-icons/tb";
import { MdOutlineDelete } from "react-icons/md";
import { useDispatch } from "react-redux";
import { deleteBlog } from "../redux/features/Blog/BlogSlice";

const BlogComponent = ({ data }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation();
  console.log(location.pathname);

  const handleDelete = (id) => {
    dispatch(deleteBlog(id))
      .unwrap()
      .then(() => navigate("/"));
  };

  return (
    <div className="flex flex-col gap-3 p-3 bg-gray-50 rounded-md hover:shadow-md">
      <img
        alt="blog-logo"
        src="https://thumbs.dreamstime.com/b/blog-information-website-concept-workplace-background-text-view-above-127465079.jpg"
        className="rounded-md w-full h-48 object-cover"
      />
      <div>
        {data.title && (
          <div className="font-medium text-lg capitalize underline">
            {data.title}
          </div>
        )}
        {data.authorName && (
          <div className="capitalize">Author: {data.authorName}</div>
        )}
        {data.description && <div>{data.description}</div>}
      </div>
      <div className="flex items-center gap-10 text-gray-500">
        <button
          className="flex items-center gap-1"
          onClick={() => navigate(`/blog/${data.id}`)}
        >
          Read blog <TbArrowUpRight size={18} />
        </button>
        <div className="flex items-center gap-2">
          <button onClick={() => navigate(`/blog/edit/${data.id}`)}>
            <TbEdit size={18} />
          </button>
          <button onClick={() => handleDelete(data.id)}>
            <MdOutlineDelete size={18} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default BlogComponent;
