import React from "react";
import { Triangle } from "react-loader-spinner";
import { useSelector } from "react-redux";

const Loader = () => {
  const { message } = useSelector((state) => state.blog);
  return (
    <div className="fixed w-full h-screen top-0 left-0 bg-black/80 flex items-center justify-center">
      <div className="flex flex-col items-center gap-2">
        <Triangle height="80" width="80" radius="9" color="white" />
        <div className="text-white">{message}</div>
      </div>
    </div>
  );
};

export default Loader;
