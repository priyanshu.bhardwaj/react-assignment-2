import React from "react";
import { Outlet } from "react-router-dom";

const Wrapper = () => {
    return (
      <div className="p-10">
        <Outlet />
      </div>
    );
};

export default Wrapper;
