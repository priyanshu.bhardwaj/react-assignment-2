import { configureStore } from "@reduxjs/toolkit";
import logger from "redux-logger";
import blogReducer from "../features/Blog/BlogSlice";

export const store = configureStore({
  reducer: {
    blog: blogReducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});