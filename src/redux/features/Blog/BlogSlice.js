import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getBlogs = createAsyncThunk(
  "blog/getBlogs",
  async (obj, { fulfillWithValue, rejectWithValue }) => {
    try {
      const response = await axios.get("http://localhost:3001/blogs");
      return fulfillWithValue(response.data);
    } catch (error) {
      return rejectWithValue(error.response.statusText);
    }
  }
);

export const deleteBlog = createAsyncThunk(
  "blog/deleteBlog",
  async (id, { fulfillWithValue, rejectWithValue, dispatch }) => {
    try {
      const response = await axios.delete(`http://localhost:3001/blogs/${id}`);
      if (response) {
        dispatch(getBlogs());
      }
    } catch (error) {
      return rejectWithValue(error.response.statusText);
    }
  }
);

export const getBlogDetails = createAsyncThunk(
  "blog/getBlogDetails",
  async (id, { fulfillWithValue, rejectWithValue }) => {
    try {
      const response = await axios.get(`http://localhost:3001/blogs/${id}`);
      return fulfillWithValue(response.data);
    } catch (error) {
      return rejectWithValue(error.response.statusText);
    }
  }
);

export const updateBlog = createAsyncThunk(
  "blog/updateBlog",
  async (data, { fulfillWithValue, rejectWithValue }) => {
    try {
      const response = await axios.put(
        `http://localhost:3001/blogs/${data.id}`,
        data
      );
      return fulfillWithValue(response.data);
    } catch (error) {
      return rejectWithValue(error.response.statusText);
    }
  }
);

export const createBlog = createAsyncThunk(
  "blog/createBlog",
  async (data, { fulfillWithValue, rejectWithValue }) => {
    try {
      const response = await axios.post(`http://localhost:3001/blogs`, data);
      return fulfillWithValue(response.data);
    } catch (error) {
      return rejectWithValue(error.response.statusText);
    }
  }
);

const initialState = {
  loading: false,
  message: "",
  error: "",
  blogs: [],
};

const blogSlice = createSlice({
  name: "blog",
  initialState,
  extraReducers: (builder) => {
    //get all blogs
    builder.addCase(getBlogs.pending, (state) => {
      state.loading = true;
      state.message = "Loading Blogs";
    });
    builder.addCase(getBlogs.fulfilled, (state, action) => {
      state.loading = false;
      state.error = "";
      state.blogs = action.payload;
      state.message = "";
    });
    builder.addCase(getBlogs.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
      state.message = "";
    });

    //delete blog
    builder.addCase(deleteBlog.pending, (state) => {
      state.loading = true;
      state.message = "Deleting Blog";
    });
    builder.addCase(deleteBlog.fulfilled, (state) => {
      state.loading = false;
      state.error = "";
      state.message = "";
    });
    builder.addCase(deleteBlog.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
      state.message = "";
    });

    //blog details
    builder.addCase(getBlogDetails.pending, (state) => {
      state.loading = true;
      state.message = "Loading Blog Details";
    });
    builder.addCase(getBlogDetails.fulfilled, (state) => {
      state.loading = false;
      state.error = "";
      state.message = "";
    });
    builder.addCase(getBlogDetails.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
      state.message = "";
    });

    //update blog
    builder.addCase(updateBlog.pending, (state) => {
      state.loading = true;
      state.message = "Updating Blog";
    });
    builder.addCase(updateBlog.fulfilled, (state) => {
      state.loading = false;
      state.error = "";
      state.message = "";
    });
    builder.addCase(updateBlog.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
      state.message = "";
    });

    //create blog
    builder.addCase(createBlog.pending, (state) => {
      state.loading = true;
      state.message = "Creating Blog";
    });
    builder.addCase(createBlog.fulfilled, (state) => {
      state.loading = false;
      state.error = "";
      state.message = "";
    });
    builder.addCase(createBlog.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
      state.message = "";
    });
  },
});

export default blogSlice.reducer;
