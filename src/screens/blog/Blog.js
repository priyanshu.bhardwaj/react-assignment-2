import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import BlogComponent from "../../components/BlogComponent";
import Loader from "../../components/Loader";
import { getBlogDetails } from "../../redux/features/Blog/BlogSlice";

const Blog = () => {
  const { id } = useParams();
  const [data, setData] = useState([]);
  const { loading } = useSelector((state) => state.blog);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBlogDetails(id))
      .unwrap()
      .then((response) => setData(response));
  }, [id, dispatch]);

  return <>{loading ? <Loader /> : <BlogComponent data={data} />}</>;
};

export default Blog;
