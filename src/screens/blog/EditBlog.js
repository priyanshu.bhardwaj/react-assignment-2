import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import {
  getBlogDetails,
  updateBlog,
  createBlog,
} from "../../redux/features/Blog/BlogSlice";
import { useFormik } from "formik";
import * as Yup from "yup";

const EditBlog = () => {
  const { id } = useParams();
  const [data, setData] = useState({
    title: "",
    authorName: "",
    description: "",
  });
  const { loading } = useSelector((state) => state.blog);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (id) {
      dispatch(getBlogDetails(id))
        .unwrap()
        .then((response) => setData(response));
    }
  }, [id, dispatch]);

  const handleSave = () => {
    console.log(data, "submitting...");
    if (!id) {
      dispatch(createBlog(data))
        .unwrap()
        .then(() => navigate("/"));
    } else {
      dispatch(updateBlog(data))
        .unwrap()
        .then(() => navigate("/"));
    }
  };

  const formik = useFormik({
    initialValues: data,
    enableReinitialize: true,
    onSubmit: handleSave,
    validationSchema: Yup.object({
      title: Yup.string().required("Title is a required field"),
      authorName: Yup.string().required("Author is a required field"),
      description: Yup.string().required("Description is a required field"),
    }),
  });

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <form
          onSubmit={formik.handleSubmit}
          className="flex flex-col justify-center gap-5"
        >
          <div className="flex flex-col gap-5 w-3/5">
            <div>
              <div className="text-gray-500 font-medium">Title</div>
              <input
                type="text"
                value={data.title}
                className="border-2 border-gray-100 rounded-md p-2 outline-none w-full"
                onChange={(e) => setData({ ...data, title: e.target.value })}
              />
              {formik.touched.title && formik.errors.title && (
                <div className="text-red-400">{formik.errors.title}</div>
              )}
            </div>
            <div>
              <div className="text-gray-500 font-medium">Author</div>
              <input
                type="text"
                value={data.authorName}
                className="border-2 border-gray-100 rounded-md p-2 outline-none w-full"
                onChange={(e) =>
                  setData({ ...data, authorName: e.target.value })
                }
              />
              {formik.touched.authorName && formik.errors.authorName && (
                <div className="text-red-400">{formik.errors.authorName}</div>
              )}
            </div>
            <div>
              <div className="text-gray-500 font-medium">Description</div>
              <textarea
                rows={5}
                type="text"
                value={data.description}
                className="border-2 border-gray-100 resize-none rounded-md p-2 outline-none w-full"
                onChange={(e) =>
                  setData({ ...data, description: e.target.value })
                }
              />
              {formik.touched.description && formik.errors.description && (
                <div className="text-red-400">{formik.errors.description}</div>
              )}
            </div>
          </div>
          <button
            type="submit"
            className="bg-blue-600 text-white py-2 px-5 rounded-md hover:bg-blue-700 w-fit"
          >
            Save Changes
          </button>
        </form>
      )}
    </>
  );
};

export default EditBlog;
