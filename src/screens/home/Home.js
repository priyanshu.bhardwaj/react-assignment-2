import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import BlogComponent from "../../components/BlogComponent";
import Loader from "../../components/Loader";
import { getBlogs } from "../../redux/features/Blog/BlogSlice";

const Home = () => {
  const { blogs, loading } = useSelector((state) => state.blog);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getBlogs());
  }, [dispatch]);

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <div>
          <div className="flex items-center justify-between">
            <div className="uppercase text-3xl font-medium text-gray-500">
              Blogs
            </div>
            <button onClick={() => navigate("/blog/new")}>
              <div className="uppercase">Create Blog</div>
            </button>
          </div>
          <div className="mt-10 grid grid-cols-3 gap-10">
            {blogs?.map((item) => {
              return <BlogComponent key={item.id} data={item} />;
            })}
          </div>
        </div>
      )}
    </>
  );
};

export default Home;
